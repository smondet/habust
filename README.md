Habust: Hackishly Build Stuff
=============================

Build
-----

Build locally:

    ocaml configure.ml
    jbuild build @install

Or to install it more globally:

    opam pin add habust . -kgit

Test/Run
--------

`_build/default/src/app/main.exe` (installed as `habust`) is the main
application, see `--help`.

There are a few test examples that the 
`_build/default/src/examples/main.exe` executable can output.

    _build/default/src/examples/main.exe owrttpd > /tmp/owr.json
    _build/default/src/app/main.exe -i /tmp/owr.json -o /tmp/habowr -S 10001

Generates a `/tmp/habowr` directory, with a “master” `Makefile`
(and a `_scripts/` directory), see:

    make help

`make start` will download files into `./_cache/` before starting `qemu`;
`make run` when successful will put its results in `./_results/`.
