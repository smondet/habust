open Nonstd
open Astring


include Build_format_t
module Environment = struct
  type t = environment
  let to_string_hum =
    function
    | Qemu (_,Openwrt ) -> "Qemu-arm-owrt"
    | Qemu (_,Debian_wheezy ) -> "Qemu-arm-debzy"
end
module Action = struct
  type t = action
  let to_string_hum =
    let cmd = String.concat ~sep:" " in
    function
    | Exec l -> strf "exec: {%s}" (cmd l)
    | Get_file (p, a) -> strf "get: '%s' as '%s'" p a
    | Get_executable {path; with_ldd; basename} ->
      strf "get-exe: '%s' (with%s ldd) as '%s*'" path
        (if with_ldd then "" else "out") basename
    | Ensure {condition; build} ->
      strf "ensure: %s with { %s }"
        begin match condition with
        | Returns_zero c -> strf "{%s} returns 0" (cmd c)
        end
        (List.map build ~f:cmd |> String.concat ~sep:" && ")
end
type t = build
let make environment recipe =
  { environment; recipe}

let serialize: t -> string = Build_format_j.string_of_build
let deserialize_exn: string -> t = Build_format_j.build_of_string

let to_string_hum {environment; recipe} =
  strf "On %s, [%s]"
    (Environment.to_string_hum environment)
    (String.concat ~sep:"; " (List.map ~f:Action.to_string_hum recipe))

module Construct = struct
  open Environment
  open Action
  let openwrt = Openwrt
  let debian_wheezy = Debian_wheezy 
  let qemu_arm v = Qemu (Arm, v)
  let within = make
  let exec l = Exec l
  let get_file f ~as_file = Get_file (f, as_file) 
  let get_executable ?(with_ldd = true) path ~dest =
    Get_executable {path; with_ldd; basename = dest}
  let ensure condition build =
    Ensure {condition; build}
  let returns_zero cmd = Returns_zero cmd
  let executables_available l =
    returns_zero [
      "sh"; "-c"; List.map l ~f:(strf "which %s") |> String.concat ~sep:" && "
    ]
  let md5 path (`Contains contains) =
    returns_zero [
      "sh"; "-c"; strf "md5sum '%s' | grep '%s'" path contains
    ]
  let file_exists p = `File_exists p
  let tests tests =
    returns_zero [
      "sh"; "-c";
      List.map tests begin function
      | `File_exists p -> strf "test -f %s" (Filename.quote p)
      end |> String.concat ~sep:" && "
    ]
end
