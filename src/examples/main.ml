
open Nonstd
open Astring
open Habust

let (//) = Filename.concat
let dbg fmt = ksprintf (eprintf "Hadbg: %s\n%!") fmt

let examples =
  [
    "owrttpd",
    Build_definition.Construct.(
      within (qemu_arm openwrt) [
        exec ["opkg"; "install"; "lighttpd"];
        get_file "/usr/sbin/lighttpd" "lighttpd-armv7l-bin";
      ]
    );
    "debzymacs",
    Build_definition.Construct.(
      within (qemu_arm debian_wheezy) [
        exec ["apt-get"; "update"];
        exec ["apt-get"; "install"; "--yes"; "emacs23"];
        get_executable "/usr/bin/emacs" ~dest:"emacs-armv7l-bin";
      ]
    );
    "debzyket",
    Build_definition.Construct.(
      let opam_bin = "/usr/local/bin/opam" in
      let root_opt = "--root=/opam-root" in
      let yes = "--yes" in
      let opam cmd l =
        [opam_bin; cmd; root_opt] @ l in
      let opam_exec l =
        opam "config" @@ ["exec"; "--"] @ l in
      let add_pin pkg url =
        opam_exec ["opam"; "pin"; "add"; pkg; yes; "-n"; url] in
      let pin_gitlab pkg =
        add_pin pkg (strf "https://gitlab.com/smondet/%s.git" pkg) in
      let opam_install l =
        opam_exec (["opam"; "install"; yes] @ l) in
      let ocaml_version = "4.06.0+trunk" in
      within (qemu_arm debian_wheezy) [
        ensure (executables_available ["unzip"; "gcc"; "make"; "git"]) [
          ["sh"; "-c";
           "echo 'deb http://ftp.debian.org/debian wheezy-backports main' >> /etc/apt/sources.list"];
          ["apt-get"; "update"];
          ["apt-get"; "install"; "--yes"; "build-essential"; "unzip"; "git"];
        ];
        ensure (md5 opam_bin (`Contains "46e25cc5b26")) [
          ["wget";
           "https://github.com/ocaml/opam/releases/download/1.2.2/opam-1.2.2-armv7l-Linux";
           "-O"; opam_bin];
        ];
        exec ["chmod"; "a+x"; "/usr/local/bin/opam"];
        ensure (tests [
            ksprintf file_exists "/opam-root/%s/bin/ocamlopt" ocaml_version
          ]) [
          ["rm"; "-rf"; "/opam-root"];
          opam "init" [yes; "--comp"; ocaml_version ];
        ];
        ensure (returns_zero @@ opam_exec ["opam-depext"; "--version"]) [
          opam_install ["depext"]; (* ; "js_of_ocaml-lwt"; "cohttp-lwt-unix"; "rresult"]; *)
        ];
        ensure (returns_zero @@ opam_exec ["vimebac"; "--version"]) [
          (* opam_exec ["opam"; "remove"; "--yes"; "ocamlfind"]; *)
          (* pin_gitlab "misuja"; *)
          pin_gitlab "vimebac";
          opam_exec ["opam"; "depext"; "--yes"; "vimebac"];
          opam_install ["vimebac"];
        ];
        get_executable
          (strf "/opam-root/%s/bin/vimebac" ocaml_version)
          ~dest:"vimebac-armv7l-bin";
        ensure (returns_zero @@ opam_exec ["vecosek"; "--version"]) [
          (* add_pin "uri" "1.9.2"; *)
          pin_gitlab "vecosek";
          opam_exec ["opam"; "depext"; "--yes"; "vecosek"];
          opam_install ["vecosek"; ];
        ];
        get_executable
          (strf "/opam-root/%s/bin/vecosek" ocaml_version)
          ~dest:"vecosek-armv7l-bin";
      ]
    );
  ]

let usage () =
  printf "usage: %s {%s}\n%!" Sys.argv.(0)
    (List.map examples ~f:(fun (n, _) -> n) |> String.concat ~sep:",");
  ()

let () =
  match (Sys.argv |> Array.to_list |> List.tl_exn) with
  | [] -> usage (); exit 0
  | more ->
    List.iter more ~f:begin fun affix ->
      match
        List.find examples ~f:(fun ex -> String.is_prefix ~affix (fst ex))
      with
      | Some (_, v) ->
        let s = Build_definition.serialize v in
        printf "%s\n%!" s
      | None ->
        usage ();
        exit 1
    end
